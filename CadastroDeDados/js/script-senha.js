/*Função que captura os dados do input e verifica se a senha tem o tamanho
    mínimo de caracteres
*/
function checarSenha(senha, confirmeSenha) {
    let senha1 = document.getElementById('senha').value
    let senha2 = document.getElementById('confirmeSenha').value

    if (senha1.length < 8) {
        alert('Senha não possui a quantidade minima de caracteres')
    } else {
        checarComplexidadeSenha(senha1)  //chamada da função que verifica a complexidade da senha
    }
}

//Função que verifica se as senhas são iguais
function compararSenhas(senha1, senha2) {
    if (senha1 != "" && senha2 != "" && senha1 === senha2) {
        alert('Senha cadastrada com sucesso');
    } else {
        alert('As senhas não coincidem');
    }
}

//Função que verifica a complexidade da senha
function checarComplexidadeSenha(senha1) {
    //if(!regex.exec(senha1)){
    if ((senha1.match(/[A-Z]+/)) && (senha1.match(/[!@#$%*]+/)) && (senha1.match(/[0-9]+/))) {
        compararSenhas() //chamada da função que verifica se as senhas são iguais

    } else {
        alert("A senha deve conter no mínimo 1 letra Maiúscula, 1 letra minúscula e 1 número");
    }
}

